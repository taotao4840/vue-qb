/*
 * @Description: 事件总线
 * @Author: jun.fu
 * @Date: 2018-06-25 16:55:21
 * @Last Modified by: jun.fu
 * @Last Modified time: 2018-06-25 16:55:42
 */
import Vue from 'Vue'

export default new Vue()
