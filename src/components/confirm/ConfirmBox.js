import ConfirmBoxVue from './ConfirmBoxVue.vue'
import Vue from 'vue'
// 参考地址
// https://www.jianshu.com/p/050b9d10e829

var ConfirmBoxConstructor = Vue.extend(ConfirmBoxVue)

let instance, boxObj
const defaultCallback = action => {
  if (action) {
    boxObj.resolve()
  } else {
    boxObj.reject()
  }
}

var ConfirmBox = function(options, callback) {
  if (!instance) {
    instance = new ConfirmBoxConstructor({
      el: document.createElement('div')
    })
    for (var prop in options) {
      if (options.hasOwnProperty(prop)) {
        instance[prop] = options[prop]
      }
    }
    if (!instance.callback) {
      instance.callback = defaultCallback
    }
  }
  document.body.appendChild(instance.$el)

  if (typeof Promise !== 'undefined') {
    return new Promise(function(resolve, reject) {
      // eslint-disable-line
      boxObj = {
        callback: callback,
        resolve: resolve,
        reject: reject
      }
      Vue.nextTick(() => {
        instance.mask.show()
      })
    })
  } else {
  }
}

export default ConfirmBox
