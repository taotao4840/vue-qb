/*
 * @Description: 组件类型定义
 * @Author: tao.xie
 * @Date: 2019-04-01 21:32:28
 * @Last Modified by: tao.xie
 * @Last Modified time: 2019-04-01 21:39:09
 */
const PAGE_HEADER = '7' // 头部
const PAGE_BOTTOM = '8' // 底部
const PIC_AND_VIDEO = '0' // 图片和视频

export { PAGE_HEADER, PAGE_BOTTOM, PIC_AND_VIDEO }
