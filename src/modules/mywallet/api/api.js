import ajaxAsync from 'vue-xiaobu-utils'

/**
 * 获取订单信息
 * @param {*} orderID 
 */
function getOrderInfo(orderID) {
  return ajaxAsync('order/query2', { ORDER_ID: orderID })
}

/**
 *获取绑定的银行卡列表
 *
 * @returns
 */
async function getBankCardList() {
  return ajaxAsync()
}

export { getOrderInfo, getBankCardList }
