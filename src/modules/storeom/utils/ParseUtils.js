function getAdIDs(name, projectMemo) {
  let adids = []
  projectMemo.map(item => {
    let cidsp = item.CID.split('_')[0]
    if (name === cidsp) {
      adids = item.AD_IDS
    }
  })
  return adids
}

function getAdList(adIds, adList) {
  if (adIds && adIds.length > 0) {
    let dl = []
    adList.map(item => {
      if (adIds.indexOf(item.AD_ID) > -1) {
        dl.push(item)
      }
    })
    return dl
  } else {
    return []
  }
}

export { getAdIDs, getAdList }
