import axios from 'axios'

/*
 * @Description: api接口
 * @Author: tao.xie
 * @Date: 2019-04-01 20:57:45
 * @Last Modified by: tao.xie
 * @Last Modified time: 2019-04-01 21:06:44
 */

async function getPageConfigs(opNum, storeID) {
  let sendObj = { OP_NUM: opNum, STORE_ID: storeID }
  let res = await axios.post(
    'http://hvms.g-armor.cn/kdshop-new-api/ad/vms/get1?token=htxf',
    sendObj,
    {
      headers: {
        'Content-Type': 'application/JSON;charset=UTF-8'
      }
    }
  )
  return res.data.BODY
}

export { getPageConfigs }
