import { MessageBox } from 'mint-ui'
import './style/messagebox.css'

function showConfirmDialog(
  title,
  message,
  confirmButtonText,
  cancelButtonText
) {
  let obj = {
    title: title,
    message: message,
    confirmButtonText: confirmButtonText,
    confirmButtonClass: 'baseBgColorWithActive'
  }
  if (cancelButtonText) {
    obj.showCancelButton = true
    obj.cancelButtonText = cancelButtonText
  }
  return MessageBox(obj)
}

export { showConfirmDialog }
