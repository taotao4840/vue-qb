import { go } from 'vue-xiaobu-utils'

function open(url) {
  if (window.isProd) {
    go(url)
    // window.location.assign('xbapp://open/' + url)
  } else {
    // window.location.assign(url)
    // alert(url)
    go(url)
  }
}

export { open }
