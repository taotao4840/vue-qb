// 项目中一些模块是否启用的配置
// 是否需要推荐商品
var NEED_RECOMMEND = false
// 是否需要在线客服
var NEED_SERVICE = false
// 是否需要积分任务
var NEED_POINT_TASK = false
// 客服电话
var SERVICE_TEL = '0519-85175888'
// 是否需要赞助商logo
var NEED_DISCOUNT_LOGO = true
// 是否需要埋点，暂用于登录模块
var NEED_RECORD_POINT = false
// 是否启用第三方广告位
var NEED_THIRD_ADS = false
// 关闭微信支付
var CLOSE_WXPAY = true
var APP_THEME_COLOR = '#3795FF'

var CARD_BALANCE_DESCRIPTION =
  'http://static.xiaobu.hk/changzx/static/cardBalanceDescription.html'

function getSmartCardDesc() {
  return ''
}

// 卡号格式化，云卡员工卡通用
function formatCardNo(cardNo) {
  var result = 'NO.</br>'
  for (var i = 0; i < cardNo.length; i = i + 4) {
    if (i + 4 < cardNo.length) { result = result + cardNo.substring(i, i + 4) + '&nbsp;&nbsp;' } else result = result + cardNo.substring(i, cardNo.length)
  }
  return result
}

// 根据城市需求获取对应的卡号
function getCardNoForUser(cardNo, cityCardNo) {
  // for 金华
  return formatCardNo(cardNo)
}
