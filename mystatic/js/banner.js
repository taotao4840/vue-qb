var u = navigator.userAgent.toLowerCase()
var isAndroid = u.indexOf('android') > -1 || u.indexOf('linux') > -1
// 以上代码做平台判断.

// 广告位名称统一配置
// 首页广告位 750*180
var APP_HOME_MIDDLE_BANNER = 'jhx30_home_middle_banner'

// 云卡申请成功页面 690*300
var APP_VIRTUAL_APPLY_SUCCESS = 'jhx_virtual_apply_success'

// 云卡、员工卡充值成功页面/电子票购票成功 690*120
var APP_CARD_RECHARGE_SUCCESS = 'app_card_recharge_success'

// 云卡刷卡成功 690*120
var APP_CARD_PAYMENT_SUCCESS = 'jhx_card_payment_success'

// 云卡充值页面 750*320
var APP_VIRTUAL_RECHARGE = 'jhx_virtual_recharge'

// 员工卡充值页面 750*320
var APP_VIRTUAL_STAFF_RECHARGE = 'jhx_virtual_staff_recharge'

// 订餐页面广告位750*240
var APP_BOOK_MEAL = 'app_book_meal'

// 订单支付成功 690*120";
var APP_PAYMENT_SUCCESS = 'jhx_payment_success'

// 公交云卡刷卡成功底部大的广告位
var APP_BUS_CARD_PAYMENT_SUCCESS = 'jhx_card_payment_success_big'

// 刷卡成功领取奖励页面的广告位
var APP_GIFT_LIST = 'app_gift_list'

// 线路详情广告位690*120
var APP_ROUTE_DETAIL = 'app_route_detail'

// 线路规划详情广告位690*120
var APP_ROUTE_PLANNING_DETAIL = 'app_route_planning_detail'

// 公交线路详情页广告位220*220
var APP_BUSROUTE_DETAIL = 'app_busroute_detail'

var APP_TICKET_DETAIL = 'app_brtticket_buy_success'

// 以下配置是第三方来源的广告位ID

// 刷卡成功页面第三方广告位ID,区分平台
var APP_CARD_PAYMENT_SUCCESS_THIRD_SOURCE = isAndroid
  ? 'jha2017103101hf'
  : 'jhi2017103101hf'
// 线路规划页面第三方广告位ID,区分平台
var APP_LINE_PLAN_THIRD_SOURCE = isAndroid
  ? 'jha2017103102hf'
  : 'jhi2017103102hf'

var TICKET_TITLE = './static/img/ticket-banner.png'
