/*
 * 公用资源文件导入（js、css）
 * @Author: jun.fu
 * @Date: 2018-06-19 09:52:54
 * @Last Modified by: tao.xie
 * @Last Modified time: 2019-04-02 00:23:47
 */
/* eslint-disable no-unused-vars */

/**
 * 区分手机以及pc，用于判断需加载的文件
 * 无法判断手机app扫码测试
 * @returns Boolean
 */
function isApp() {
  if (/win/i.test(navigator.platform)) {
    return false
  } else {
    if (/(iPhone|iPad|iPod|iOS|Android|Linux)/i.test(navigator.userAgent)) {
      return true
    } else {
      return false
    }
  }
}

// 用于区分调试还是生产的参数
window.prodInApp = false
// 平台：true为app，false为web
var platform = true

// 可替换跳转路径中的bus-app（因为一开始该项目的路径不在bus-app下）
var busPathVue = 'bus-app'

var vuePath = './mystatic/'
var appPath = '../'

var basePath = window.prodInApp ? appPath : vuePath

// var basePath = './mystatic/'

var resume = true
var pause = false

var scriptJs = [
  'api.js',
  'cordova.js',
  window.prodInApp ? 'js/vue.min.js' : 'js/vue.js',
  // 'js/vue-router.min.js',
  // 'js/vuex.min.js',
  'js/appmoduleconfig.js',
  'js/axios.min.js',
  // 'js/flexible.js',
  'js/sign.min.js',
  // 'js/fastclick.min.js',
  'js/routermenu.js',
  'js/mtools.js',
  'js/banner.js'
]

var linkCss = [
  'css/public.css',
  'css/flex.css',
  'css/skin.css',
  'css/iconfont.css'
]

var jsArrayLen = scriptJs.length
for (var j = 0; j < jsArrayLen; j++) {
  document.write(
    "<script src='" +
      basePath +
      scriptJs[j] +
      "' type='text/javascript'></script>"
  )
}

document.write(
  "<script src='./mystatic/js/cityConfig.js' type='text/javascript'></script>"
)

var cssArrayLen = linkCss.length
for (var i = 0; i < cssArrayLen; i++) {
  document.write(
    '<link rel="stylesheet" href="' +
      basePath +
      linkCss[i] +
      '" type="text/css"/>'
  )
}
