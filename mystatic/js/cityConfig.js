// 卡片名称
var buscardName = '电子市民卡'
// 扣款失败达到上限次数的提示
var takeBusFailHint =
  '乘车扣款失败时，请您手动支付乘车金额，不处理将无法享受' +
  buscardName +
  '的扫码乘车服务。'
// 卡列表协议地址
var busCardListPostUrl = '/vcard/info/list'

// 根据城市需求获取对应的卡号
var getCardNoForUser = function(cardNo, cityCardNo) {
  // for 镇江
  // return cityCardNo.substring(8, cityCardNo.length - 1)
  // 普通城市
  return cardNo
}
