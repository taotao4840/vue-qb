/* eslint-disable no-unused-vars */
// 打包版本号配置,α版本为内部测试版本和β版本为上线版本
var versions = 'α'

// 内网测试环境API路径配置及Token配置
var api = 'http://112.13.95.178:7080/city-api'
var token = 'jhx'
// 内网开发
// var api = 'http://60.190.227.169:17080/city-api'
// var token = 'cityapp'

// 外网测试API路径配置及Token配置
// var api = 'https://alpha.xiaobu.hk/city-api'
// var token = 'jhx20'

// 外网正式β版API路径配置及Token配置
// var awardapi = 'https://api.xiaobu.hk/activity-api'
// var api = 'https://api.xiaobu.hk/city-api'
// var token = 'jhx20'

// 是否是产品模式,分享配置用,beta版本打包时需改成true
var isProd = false

// 是否启用请求签名
var usesign = true

// 是否打开页面统计
var pageStatistics = true

var source = 'GoogleMarket' // 用户来源

var cityname = '金华' // 城市名
var cityappname = '金华行' // APP名

var logdebug = true // 是否toast debug级别的日志
var toastpagenotifylog = false // 底层测试页面通知时的toast，需要的时候改成true

// var test = true // 是否是测试，true为测试
var platform = true // 平台：true为app，false为web

// var busPathVue = 'vue'

// export {
//   api,
//   token,
//   isProd,
//   usesign,
//   pageStatistics,
//   source,
//   cityname,
//   cityappname,
//   logdebug,
//   toastpagenotifylog
// }
